Source: emcee
Section: python
Priority: optional
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Build-Depends: debhelper (>= 9),
               dh-python,
               python-all (>= 2.7),
               python-numpy,
               python3-all,
               python3-numpy
Standards-Version: 3.9.8
Homepage: http://dan.iel.fm/emcee/
Vcs-Git: https://salsa.debian.org/debian-astro-team/emcee.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/emcee
X-Python-Version: >= 2.7

Package: python-emcee
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, python-numpy
Description: Affine-invariant ensemble MCMC sampling for Python
 emcee is an extensible, pure-Python implementation of Goodman &
 Weare's Affine Invariant Markov chain Monte Carlo (MCMC) Ensemble
 sampler. It's designed for Bayesian parameter estimation.
 .
 This is the Python 2 package.

Package: python3-emcee
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-numpy
Description: Affine-invariant ensemble MCMC sampling for Python 3
 emcee is an extensible, pure-Python implementation of Goodman &
 Weare's Affine Invariant Markov chain Monte Carlo (MCMC) Ensemble
 sampler. It's designed for Bayesian parameter estimation.
 .
 This is the Python 3 package.
